﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level : MonoBehaviour {

    //public int[] GroupList = new int[20]; //a list of 20 ints ranging from 1-4, determining group spawn order. If int is 0, 
    public List<int> GroupList = new List<int>();//list of ints ranging from 0-3
    public int GlobalHealth;
    public int GroupsDisappeared; //keeps track of how many groups have died or reached goal
    public int RespawnMin;
    public int RespawnMax;
    public int NextLevel;

    public Canvas Canvas; //reference to the canvas, to be passed on to groups
    public CharactersAudioManager CharactersAudioManager;
    public GameObject Group0;
    public GameObject Group1;
    public GameObject Group2;
    public GameObject Group3;
    public GameObject healthBar;

    public Transform Beginning;

    private int groupListPosition; //keeps track of position in group list
    private bool spawn; //true when a group spawn should occur
    
    private Slider healthBarSlider;

    private void Awake()
    {
        Group0.GetComponent<GroupController>().canvas = this.Canvas;
        Group0.GetComponent<GroupController>().LevelManager = this;
        Group0.GetComponent<GroupController>().CharactersAudioManager = CharactersAudioManager;
        Group1.GetComponent<GroupController>().canvas = this.Canvas;
        Group1.GetComponent<GroupController>().LevelManager = this;
        Group1.GetComponent<GroupController>().CharactersAudioManager = CharactersAudioManager;
        Group2.GetComponent<GroupController>().canvas = this.Canvas;
        Group2.GetComponent<GroupController>().LevelManager = this;
        Group2.GetComponent<GroupController>().CharactersAudioManager = CharactersAudioManager;
        Group3.GetComponent<GroupController>().canvas = this.Canvas;
        Group3.GetComponent<GroupController>().LevelManager = this;
        Group3.GetComponent<GroupController>().CharactersAudioManager = CharactersAudioManager;

        healthBarSlider = healthBar.GetComponent<Slider>();
        healthBarSlider.maxValue = GlobalHealth;
        healthBarSlider.minValue = 0;

        
    }

    // Use this for initialization
    void Start () {
        groupListPosition = 0;
        
        spawn = false;
        StartCoroutine(SetInitialSpawn());
	}
	
	// Update is called once per frame
	void Update () {
        healthBarSlider.value = GlobalHealth;
		if(GlobalHealth <= 0)
        {
            //end level
            GetComponent<LevelLoader>().LoadOnClick(3);
        }
        else
        {
            if (spawn)
            {
                if(groupListPosition < GroupList.Count)
                {
                    int n = GroupList[groupListPosition];
                    switch (n)
                    {
                        case 0:
                            Instantiate(Group0, Beginning.position, Quaternion.identity);
                            StartCoroutine(SetSpawn());
                            break;
                        case 1:
                            Instantiate(Group1, Beginning.position, Quaternion.identity);
                            StartCoroutine(SetSpawn());
                            break;
                        case 2:
                            Instantiate(Group2, Beginning.position, Quaternion.identity);
                            StartCoroutine(SetSpawn());
                            break;
                        case 3:
                            Instantiate(Group3, Beginning.position, Quaternion.identity);
                            StartCoroutine(SetSpawn());
                            break;
                        default:
                            Debug.Log("invalid int value in group list");
                            break;
                    }
                    groupListPosition++;
                }
            }
        }
	}


    private IEnumerator SetSpawn()
    {
        spawn = false;
        yield return new WaitForSeconds(Random.Range(RespawnMin, RespawnMax));
        spawn = true;
    }

    private IEnumerator SetInitialSpawn()
    {
        spawn = false;
        yield return new WaitForSeconds(2f);
        spawn = true;
    }

    public void SetGlobalHealth(int HealthValue)
    {
        GlobalHealth -= HealthValue;
    }

    public void SetGroupsDisappeared()
    {
        GroupsDisappeared++;
        CheckForLevelComplete();
    }

    public void CheckForLevelComplete()
    {
        if(GroupsDisappeared == GroupList.Count)
        {
            GetComponent<LevelLoader>().LoadOnClick(NextLevel);
        }
    }
}
