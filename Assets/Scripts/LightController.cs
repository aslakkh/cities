﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour {

    /*lightposts have four states:
     * unlit
     * unlit selected
     * lit selected
     * Lit
    */

    //public GameObject SpotLight;

    public float SpotlightRange;
    public float SpotLightIntensity;
    public Transform Lamp;

    private Light spotlight;
    public int LightSourceValue; //each light source has a determined value. During each level, a certain maximum light-soruce value exists.

    public bool rotatable;
    public float MaxRotation;
    public float RotationSpeed;

    //controls
    public string Rotate;

    //audio sources

    private AudioSource initialAudio;
    private AudioSource loopAudio;
    private AudioSource[] audiosources;

    private bool selected;
    
    private bool lit;
    private bool isFlickering;
    private bool powerAtThreshold; //power level is almost overridden. Light sources should blink.
    //private Light spotlight;
    private PolygonCollider2D lightCollider;

    private void Awake()
    {
        spotlight = GetComponentInChildren<Light>();
        if (rotatable)
        {
            lightCollider = Lamp.GetComponent<PolygonCollider2D>();
        }
        else
        {
            lightCollider = GetComponent<PolygonCollider2D>();
        }
        audiosources = GetComponents<AudioSource>();
        initialAudio = audiosources[0];
        try
        {
            loopAudio = audiosources[1];
        }
        catch
        {

        }
        


    }

    // Use this for initialization
    void Start () {
        //SpotLight.GetComponent<Light>();
        
        //if (rotatable)
        //{
        //    spotlight = GetComponentInChildren<Light>();
        //    lightCollider = Lamp.GetComponent<PolygonCollider2D>();
        //}
        //else
        //{
        //    lightCollider = GetComponent<PolygonCollider2D>();
        //}
        powerAtThreshold = false;
        

    }

    void Update()
    {
        if (rotatable && selected)
        {
            if (Input.GetAxisRaw(Rotate) > 0)
            {
                //rotate right
                if (Lamp.rotation.z < MaxRotation)
                {
                    Lamp.Rotate(0f, 0f, RotationSpeed);
                }

            }
            else if (Input.GetAxisRaw(Rotate) < 0)
            {
                //rotate left
                if (Lamp.rotation.z > -MaxRotation)
                {
                    Lamp.Rotate(0f, 0f, -RotationSpeed);
                }

            }
        }
        if(powerAtThreshold && lit && !isFlickering)
        {
            //turn on and off sometimes
            StartCoroutine(Flicker());
        }
        
    }

    private IEnumerator Flicker() //flickers light 
    {
        isFlickering = true;
        FlickerLight(false);
        yield return new WaitForSeconds(Random.Range(0.1f, 0.2f));
        FlickerLight(true);
        yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));
        isFlickering = false;
    }


    public void Select()
    {
        selected = true;
        if (lit)
        {
            spotlight.intensity = SpotLightIntensity + 1f;
        }
        else
        {
            spotlight.range = 0.6f;
            spotlight.intensity = SpotLightIntensity + 5f;
        }

        
        
    }

    public void Deselect()
    {
        selected = false;
        if (!lit)
        {
            spotlight.range = 0f;
        }
        spotlight.intensity = SpotLightIntensity;

        

    }

    public void SetLight(bool on)
    {
        if (on)
        {
            spotlight.range = SpotlightRange;
            spotlight.intensity = SpotLightIntensity + 1f;
            //lightCollider.enabled = true;
            lightCollider.offset = Vector2.zero;
            lit = true;
            initialAudio.Play();
            loopAudio.Play();
            //StartCoroutine(PlayLoop());
            //spotlight.enabled = true;
        }
        else
        {
            spotlight.range = selected ? 0.6f : 0f;
            spotlight.intensity = SpotLightIntensity + 5f;
            //lightCollider.enabled = false;
            lightCollider.offset = new Vector2(0f, 5f);
            lit = false;
            loopAudio.Stop();

        }
    }

    private IEnumerator PlayLoop()
    {
        yield return new WaitForSeconds(1f);
        loopAudio.Play();
    }

    public void FlickerLight(bool on)
    {
        if (on)
        {
            spotlight.range = SpotlightRange;
            spotlight.intensity = SpotLightIntensity + 1f;
            lightCollider.offset = Vector2.zero;
        }
        else
        {
            spotlight.range = selected ? 0.6f : 0f;
            spotlight.intensity = SpotLightIntensity + 5f;
            lightCollider.offset = new Vector2(0f, 5f);
        }
    }

    public bool GetLit()
    {
        return lit;
    }

    public bool GetIsFlickering()
    {
        return isFlickering;
    }

    public void SetIsFlickering(bool isFlickering)
    {
        this.isFlickering = isFlickering;
    }

    public void SetPowerAtThreshold(bool val)
    {
        powerAtThreshold = val;
    }
}
