﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanController : MonoBehaviour {

    //controls
    public string MovementButton;

    //public variables
    public float MovementSpeed;
    public float Health;

    private float xVelocity;
    //private Rigidbody2D rb2D;
    private Animator animator;

	// Use this for initialization
	void Start () {
        xVelocity = 0;
        //rb2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        animator.SetBool("Running", true);

    }
	
	// Update is called once per frame
	//void Update () {
 //       //if (Input.GetButton(MovementButton))
 //       //{
 //       //    xVelocity = MovementSpeed;
 //       //    animator.SetBool("Running", true);
 //       //}
 //       //else
 //       //{
 //       //    xVelocity = 0f;
 //       //    animator.SetBool("Running", false);
 //       //}

 //       //movementbutton currently only used to start movement
 //       //if (Input.GetButtonDown(MovementButton))
 //       //{
 //       //    //xVelocity = MovementSpeed;
 //       //    //animator.SetBool("Running", true);
 //       //}
	//}

    void FixedUpdate()
    {
        //rb2D.velocity = new Vector2(xVelocity, rb2D.velocity.y);
        transform.position = GetComponentInParent<Transform>().position;
    }
}
