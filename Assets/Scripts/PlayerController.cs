﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    /*
        The player controls the game by navigating between lightsources (using left and right arrows) and activating or deactivating light (using up or down arrows)
    */

    public int BlackoutTime;
    public int MaxActiveLightSources; //how many sources are allowed to be lit simultaneously. Some light sources count as two or more.
    public GameObject[] LightSources = new GameObject[20];

    //controls
    public string LeftButton, RightButton, ActivateLightButton, DeactivateLightButton;


    //private variables
    private int lightSourcesPosition;
    private LightController selectedLightSource;
    private int lightSourcesLit; //keeps track of how many lightsources are currently lit;
    private bool blackout; //all lights and control is removed during blackout

    private AudioSource warningLoop; //FIRST AUDIO SOURCE
    private AudioSource blackoutSound;

    private AudioSource[] audiosources;

    private void Awake()
    {
        audiosources = GetComponents<AudioSource>();
        warningLoop = audiosources[0];
        try
        {
            blackoutSound = audiosources[1];
        }
        catch
        {

        }
        
    }

    // Use this for initialization
    void Start()
    {
        lightSourcesPosition = 0;
        SelectNewLightSource();
        selectedLightSource.SetLight(true);
        lightSourcesLit += selectedLightSource.LightSourceValue;
    }

    // Update is called once per frame
    void Update()
    {
        if (!blackout)
        {
            if (Input.GetButtonDown(LeftButton))
            {
                if (lightSourcesPosition > 0 && LightSources[lightSourcesPosition - 1] != null)
                {
                    //deselect current and select previous
                    lightSourcesPosition--;
                    selectedLightSource.Deselect();
                    SelectNewLightSource();
                }

            }
            else if (Input.GetButtonDown(RightButton))
            {
                if (lightSourcesPosition < LightSources.Length - 1 && LightSources[lightSourcesPosition + 1] != null)
                {
                    //deselect current and select next
                    lightSourcesPosition++;
                    selectedLightSource.Deselect();
                    SelectNewLightSource();
                }
            }
            else if (Input.GetButtonDown(ActivateLightButton))
            {
                if (!selectedLightSource.GetLit())
                {
                    lightSourcesLit += selectedLightSource.LightSourceValue;
                    if (lightSourcesLit > MaxActiveLightSources) //turn off all other light sources
                    {
                        blackoutSound.Play();
                        if (warningLoop.isPlaying)
                        {
                            warningLoop.Stop();
                        }
                        for (int i = 0; i < LightSources.Length; i++)
                        {
                            if (LightSources[i] == null) //reached end of list
                            {
                                break;
                            }
                            else
                            {
                                LightController l = LightSources[i].GetComponent<LightController>();
                                if (l.GetLit()) //the light is currently lit and should be turned off
                                {
                                    if (l.GetIsFlickering())
                                    {
                                        l.StopAllCoroutines();
                                        l.SetIsFlickering(false);
                                    }
                                    l.SetLight(false);
                                }
                                l.SetPowerAtThreshold(false);
                            }
                        }
                        lightSourcesLit = 0; //reset number of lightsources lit
                        selectedLightSource.Deselect();
                        StartCoroutine(SetBlackout());
                    }
                    else
                    {
                        selectedLightSource.SetLight(true);
                        if (lightSourcesLit == MaxActiveLightSources)
                        {
                            //init flickering
                            warningLoop.Play();
                            SetPowerAtThreshold(true);
                        }

                    }

                }

            }
            else if (Input.GetButtonDown(DeactivateLightButton))
            {
                if (selectedLightSource.GetLit())
                {
                    if (lightSourcesLit == MaxActiveLightSources)
                    {
                        //stop flickering
                        if (warningLoop.isPlaying)
                        {
                            warningLoop.Stop();
                        }
                        SetPowerAtThreshold(false);
                    }
                    lightSourcesLit -= selectedLightSource.LightSourceValue;
                    selectedLightSource.SetLight(false);
                }

            }
        }

    }

    private void SelectNewLightSource()
    {
        selectedLightSource = LightSources[lightSourcesPosition].GetComponent<LightController>();
        selectedLightSource.Select();
    }

    private IEnumerator SetBlackout()
    {
        blackout = true;
        yield return new WaitForSeconds(BlackoutTime);
        blackout = false;
        SelectNewLightSource();
    }



    public void SetPowerAtThreshold(bool val)
    {
        for (int i = 0; i < LightSources.Length; i++)
        {
            if (LightSources[i] == null) //reached end of list
            {
                break;
            }
            else
            {
                LightController l = LightSources[i].GetComponent<LightController>();
                l.SetPowerAtThreshold(val);
            }
        }
    }
}