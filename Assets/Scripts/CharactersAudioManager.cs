﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharactersAudioManager : MonoBehaviour {

    private AudioSource spawnAudio;
    private AudioSource goalAudio;
    private AudioSource deathAudio;

    AudioSource[] audiosources;

    private void Awake()
    {
        audiosources = GetComponents<AudioSource>();
        spawnAudio = audiosources[0];
        try
        {
            goalAudio = audiosources[1];
            deathAudio = audiosources[2];
        }
        catch
        {

        }
    }


    public void PlaySpawnAudio()
    {
        spawnAudio.Play();
    }

    public void PlayGoalAudio()
    {
        goalAudio.Play();
    }

    public void PlayDeathAudio()
    {
        deathAudio.Play();
    }
	
}
