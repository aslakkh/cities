﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GroupController : MonoBehaviour {

    //TODO:
    //- Remove health when not inside light collider

    public int Health;
    public int Damage; //how much damage this group takes from the global health value if it dies
    public float MovementSpeed;
    public GameObject HealthBarPrefab; //slider prefab
    public LayerMask LightLayer;
    public LayerMask GoalLayer;
    public Canvas canvas;
    public Level LevelManager;

    public CharactersAudioManager CharactersAudioManager;

    //controls
    public string MovementButton;


    //private variables
    private bool lit; //group is under light
    private Rigidbody2D rb2D;
    private Collider2D boxCollider;
    private float xVelocity;
    private GameObject healthBar;
    private Slider healthBarSlider;

	// Use this for initialization
	void Start () {
        rb2D = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<Collider2D>();
        xVelocity = MovementSpeed;
        healthBar = Instantiate(HealthBarPrefab);
        healthBarSlider = healthBar.GetComponent<Slider>();
        healthBar.transform.SetParent(canvas.transform);
        healthBarSlider.maxValue = Health;
        healthBarSlider.minValue = 0;
        healthBar.transform.position = new Vector3(transform.position.x, transform.position.y - 0.2f, transform.position.z);
        CharactersAudioManager.PlaySpawnAudio();
	}
	
	// Update is called once per frame
	void Update () {
        //if (Input.GetButtonDown(MovementButton))
        //{
        //    xVelocity = MovementSpeed; //hardcode, fix to match with movement of individual characters
        //}
        healthBarSlider.value = Health;
        
    }

    private void FixedUpdate()
    {
        rb2D.velocity = new Vector2(xVelocity, 0f);
        //lit = boxCollider.IsTouchingLayers(LightLayer);
        if (!lit)
        {
            Health--;
        }
        healthBar.transform.position = new Vector3(transform.position.x, transform.position.y - 0.2f, transform.position.z);
        if(Health < 0)
        {
            //die
            Die();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(GoalLayer == (GoalLayer | (1 << collision.gameObject.layer)))
        {
            CharactersAudioManager.PlayGoalAudio();
            LevelManager.SetGroupsDisappeared();
            healthBarSlider.fillRect.sizeDelta = Vector2.zero;
            Destroy(healthBar);
            Destroy(gameObject);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (LightLayer == (LightLayer | (1 << collision.gameObject.layer)))
        {
            lit = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (LightLayer == (LightLayer | (1 << collision.gameObject.layer)))
        {
            lit = false;
        }
    }

    private void Die()
    {
        CharactersAudioManager.PlayDeathAudio();
        LevelManager.SetGroupsDisappeared();
        healthBarSlider.fillRect.sizeDelta = Vector2.zero;
        Destroy(healthBar);
        Destroy(gameObject);
        //take life from global health
        LevelManager.SetGlobalHealth(Damage);
    }
}
